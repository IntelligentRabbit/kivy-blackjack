from kivymd.uix.screen import MDScreen
from kivymd.uix.button import MDRaisedButton
from kivy.metrics import sp

from io_data import save
from dialog import MyMDDialog


class SettingsScreen(MDScreen):
    dialog = None

    def sound_switch(self):
        if self.ids.sound_switch.active:
            self.game.sound_on = True
        else:
            self.game.sound_on = False
        self.game.sound_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def music_switch(self):
        if self.ids.music_switch.active:
            self.game.music_on = True
        else:
            self.game.music_on = False
        self.game.music_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def switch_to_game_scr(self):
        self.game.current = "game_scr"
        self.game.sound_click.play()

    def show_reset_dialog(self):
        if not self.dialog:
            self.dialog = MyMDDialog(
                auto_dismiss=False,
                title="Are you sure?",
                radius=[36, 36, 36, 36],
                icon="exclamation-thick",
                icon_color="red",
                text="You'll return to level 0 and your balance will be reset to 2000",
                font_size=sp(16),
                btn_pos="center",
                buttons=[
                    MDRaisedButton(
                        size_hint_x=0.4,
                        text="Cancel",
                        font_name="Rabbit",
                        md_bg_color="1C88E0",
                        on_release=lambda _: self.cancel_btn_press(),
                    ),
                    MDRaisedButton(
                        size_hint_x=0.4,
                        text="Reset",
                        font_name="Rabbit",
                        md_bg_color="1C88E0",
                        on_release=lambda _: self.reset_btn_press(),
                    ),
                ],
            )
        self.game.sound_click.play()
        self.dialog.open()

    def cancel_btn_press(self):
        self.game.sound_click.play()
        self.dialog.dismiss()

    def reset_btn_press(self):
        self.game.sound_click.play()
        self.dialog.dismiss()
        self.game.money = 2000
        self.game.number_of_deals = 0
        save(
            "saves",
            money=self.game.money,
            deals=self.game.number_of_deals,
        )
        self.game.get_screen("themes_scr").card_back_0_press()
        self.game.get_screen("themes_scr").card_face_0_press()
