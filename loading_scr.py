from kivymd.uix.screen import  MDScreen
from kivy.clock import Clock


class LoadingScreen(MDScreen):
    events = []

    def on_enter(self):
        self.ids.progress.start()
        Clock.schedule_once(self.game.load_music, 10)
        event = Clock.schedule_interval(self.loading_music_check, 0.1)
        self.events.append(event)

    def loading_music_check(self, dt):
        if len(self.game.music_list) == 1 and not self.game.music_loaded:
            for event in self.events:
                event.cancel()
            self.game.music_mute_unmute()
            self.game.music_loaded = True
            self.ids.progress.stop()
            self.game.current = "game_scr"
