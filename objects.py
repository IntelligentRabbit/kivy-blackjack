from random import shuffle

from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.behaviors import CommonElevationBehavior
from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder
from kivy.properties import (
    ColorProperty,
    StringProperty,
    BooleanProperty,
    NumericProperty,
)

Builder.load_file("progressbar.kv")


class Card(CommonElevationBehavior, MDFloatLayout):
    suit_image = StringProperty("")
    rank_text = StringProperty("")
    rank_text_color = ColorProperty()
    face_up = BooleanProperty(False)

    def __init__(self, rank="A", suit="Spades", x=0, y=0, **kwargs):
        super(Card, self).__init__(**kwargs)
        self.rank = rank
        self.suit = suit
        self.x = x
        self.y = y

        self.suit_image = f"images/card/{self.suit}.png"
        self.rank_text = self.rank

        if self.suit in ["Clubs", "Spades"]:
            self.rank_text_color = "black"
        else:
            self.rank_text_color = "red"


class Deck:
    def __init__(self):
        self.cards = []
        self.build()
        self.shuffle()

    def build(self):
        card_categories = ["Hearts", "Diamonds", "Clubs", "Spades"]
        cards_list = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
        self.cards = [
            (card, category) for category in card_categories for card in cards_list
        ]

    def shuffle(self):
        shuffle(self.cards)

    def deal(self):
        if len(self.cards) > 1:
            return self.cards.pop()


class Hand:
    def __init__(self):
        self.cards = []
        self.value = 0

    def add_card(self, card):
        self.cards.append(card)

    def calc_hand(self):
        self.value = 0

        first_card_index = [a_card[0] for a_card in self.cards]
        non_aces = [c for c in first_card_index if c != "A"]
        aces = [c for c in first_card_index if c == "A"]

        for card in non_aces:
            if card in "JQK":
                self.value += 10
            else:
                self.value += int(card)

        for card in aces:
            if self.value <= 10:
                self.value += 11
            else:
                self.value += 1

        return self.value


class LevelLayout(MDFloatLayout):
    level_label = StringProperty("")
    level_pb = NumericProperty(0)
