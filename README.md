![https://img.shields.io/badge/Python-3.9.7-blue](https://img.shields.io/badge/Python-3.9.7-blue) ![https://img.shields.io/badge/code%20style-black-000000.svg](https://img.shields.io/badge/code%20style-black-000000.svg)

# :heart: Kivy Blackjack :spades:

Mobile game for Android in Python with Kivy framework and KivyMD library.

Watch gameplay on [YouTube](https://youtu.be/xu2kC522NmE?si=iFAqO8gfh6KaGTmg)

# Prerequisites
- [Kivy>=2.0.0](https://kivy.org/#home)
- [KivyMD>=1.1.1](https://kivymd.readthedocs.io/en/latest/)
