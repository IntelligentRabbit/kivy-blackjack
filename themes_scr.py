from kivymd.uix.screen import MDScreen
from kivymd.uix.card import MDCard
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.floatlayout import MDFloatLayout
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
    ListProperty,
    ColorProperty,
)

from io_data import save


class CardBack(MDCard):
    image = StringProperty("")
    unlock_level = NumericProperty(0)
    unlock = BooleanProperty(False)
    in_use = BooleanProperty(False)

    def unlock_card_back(self, level):
        if self.unlock_level <= level:
            self.unlock = True


class CardFace(MDCard):
    image = StringProperty("images/card/rabbit.png")
    unlock_level = NumericProperty(0)
    unlock = BooleanProperty(False)
    in_use = BooleanProperty(False)
    rank = StringProperty("A")
    suit = StringProperty("Spades")
    suit_image = StringProperty("")

    def unlock_card_face(self, level):
        if self.unlock_level <= level:
            self.unlock = True


class Tab(MDFloatLayout, MDTabsBase):
    pass


class ThemesScreen(MDScreen):
    card_back_list = ListProperty([])
    card_back_0 = ObjectProperty(None)
    card_back_12 = ObjectProperty(None)
    card_back_25 = ObjectProperty(None)
    card_back_37 = ObjectProperty(None)
    card_back_49 = ObjectProperty(None)
    card_back_62 = ObjectProperty(None)
    card_back_74 = ObjectProperty(None)
    card_back_86 = ObjectProperty(None)
    card_back_99 = ObjectProperty(None)
    card_face_list = ListProperty([])
    card_face_0 = ObjectProperty(None)
    card_face_12 = ObjectProperty(None)
    card_face_25 = ObjectProperty(None)
    card_face_37 = ObjectProperty(None)
    card_face_49 = ObjectProperty(None)
    card_face_62 = ObjectProperty(None)
    card_face_74 = ObjectProperty(None)
    card_face_86 = ObjectProperty(None)
    card_face_99 = ObjectProperty(None)

    def on_pre_enter(self):
        self.card_back_list = [
            self.card_back_0,
            self.card_back_12,
            self.card_back_25,
            self.card_back_37,
            self.card_back_49,
            self.card_back_62,
            self.card_back_74,
            self.card_back_86,
            self.card_back_99,
        ]
        self.card_face_list = [
            self.card_face_0,
            self.card_face_12,
            self.card_face_25,
            self.card_face_37,
            self.card_face_49,
            self.card_face_62,
            self.card_face_74,
            self.card_face_86,
            self.card_face_99,
        ]

        for card_back in self.card_back_list:
            card_back.unlock_card_back(self.game.level)
            if card_back.image == self.game.card_back_image:
                card_back.in_use = True

        for card_face in self.card_face_list:
            card_face.unlock_card_face(self.game.level)
            if card_face.image == self.game.card_face_image:
                card_face.in_use = True

    def card_back_press(self, card_back_x):
        if card_back_x.unlock:
            card_back_x.in_use = True
            self.game.card_back_image = card_back_x.image
            save(
                "saves_card",
                card_back_image=self.game.card_back_image,
                card_face_image=self.game.card_face_image,
            )
            for card_back in self.card_back_list:
                if card_back != card_back_x:
                    card_back.in_use = False

    def card_back_0_press(self):
        self.card_back_press(self.card_back_0)

    def card_back_12_press(self):
        self.card_back_press(self.card_back_12)

    def card_back_25_press(self):
        self.card_back_press(self.card_back_25)

    def card_back_37_press(self):
        self.card_back_press(self.card_back_37)

    def card_back_49_press(self):
        self.card_back_press(self.card_back_49)

    def card_back_62_press(self):
        self.card_back_press(self.card_back_62)

    def card_back_74_press(self):
        self.card_back_press(self.card_back_74)

    def card_back_86_press(self):
        self.card_back_press(self.card_back_86)

    def card_back_99_press(self):
        self.card_back_press(self.card_back_99)

    def card_face_press(self, card_face_x):
        if card_face_x.unlock:
            card_face_x.in_use = True
            self.game.card_face_image = card_face_x.image
            save(
                "saves_card",
                card_back_image=self.game.card_back_image,
                card_face_image=self.game.card_face_image,
            )
            for card_face in self.card_face_list:
                if card_face != card_face_x:
                    card_face.in_use = False

    def card_face_0_press(self):
        self.card_face_press(self.card_face_0)

    def card_face_12_press(self):
        self.card_face_press(self.card_face_12)

    def card_face_25_press(self):
        self.card_face_press(self.card_face_25)

    def card_face_37_press(self):
        self.card_face_press(self.card_face_37)

    def card_face_49_press(self):
        self.card_face_press(self.card_face_49)

    def card_face_62_press(self):
        self.card_face_press(self.card_face_62)

    def card_face_74_press(self):
        self.card_face_press(self.card_face_74)

    def card_face_86_press(self):
        self.card_face_press(self.card_face_86)

    def card_face_99_press(self):
        self.card_face_press(self.card_face_99)

    def switch_to_game_scr(self):
        self.game.current = "game_scr"
        self.game.sound_click.play()
