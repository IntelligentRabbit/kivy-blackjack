import sys

from kivymd.app import MDApp
from kivymd.uix.button import MDRaisedButton
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.properties import (
    NumericProperty,
    BooleanProperty,
    StringProperty,
    ObjectProperty,
    ListProperty,
)
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.core.text import LabelBase
from kivy.animation import Animation
from kivy.metrics import dp, sp
from kivy.core.window import Window

from objects import Card, Deck, Hand, LevelLayout
from io_data import save, load
from dialog import MyMDDialog
from settings_scr import SettingsScreen
from themes_scr import ThemesScreen
from loading_scr import LoadingScreen

VERSION = "v. 1.0.0"

Builder.load_file("loading_scr.kv")
Builder.load_file("settings_scr.kv")
Builder.load_file("themes_scr.kv")
Builder.load_file("objects.kv")


class BetLayout(BoxLayout):
    image = StringProperty("")
    bet = StringProperty("")


class RewardLayout(BoxLayout):
    pass


class Game(ScreenManager):
    money = NumericProperty(2000)
    sound_on = BooleanProperty(True)
    music_on = BooleanProperty(True)
    sound_list = ListProperty([])
    music_list = ListProperty([])
    level = NumericProperty(0)
    level_pb = NumericProperty(0)
    number_of_deals = NumericProperty(0)
    card_back_image = StringProperty("images/card/card_back/cb1.png")
    card_face_image = StringProperty("images/card/card_face/cf1.png")
    music_loaded = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.sound_click = SoundLoader.load("sounds/btn-press.wav")
        self.sound_card = SoundLoader.load("sounds/card-placement.wav")
        self.sound_chip = SoundLoader.load("sounds/chip.wav")
        self.sound_win = SoundLoader.load("sounds/win.wav")
        self.sound_lost = SoundLoader.load("sounds/lost.wav")
        self.sound_push = SoundLoader.load("sounds/push.wav")
        self.sound_blackjack = SoundLoader.load("sounds/magical-coin-win.wav")
        self.sound_list = [
            self.sound_click,
            self.sound_card,
            self.sound_chip,
            self.sound_win,
            self.sound_lost,
            self.sound_push,
            self.sound_blackjack,
        ]
        
        saves_settings = load("saves_settings")
        if len(saves_settings) != 0:
            self.music_on = saves_settings["music_on"]
            self.sound_on = saves_settings["sound_on"]

        self.sound_mute_unmute()
        self.music_mute_unmute()

        saves = load("saves")
        if len(saves) != 0:
            self.money = saves["money"]
            self.number_of_deals = saves["deals"]

        saves_card = load("saves_card")
        if len(saves_card) != 0:
            self.card_back_image = saves_card["card_back_image"]
            self.card_face_image = saves_card["card_face_image"]

    def sound_mute_unmute(self):
        if self.sound_on:
            for sound in self.sound_list:
                sound.volume = 1
        else:
            for sound in self.sound_list:
                sound.volume = 0

    def music_mute_unmute(self):
        if self.music_on:
            for music in self.music_list:
                music.volume = 1
        else:
            for music in self.music_list:
                music.volume = 0

    def calc_level(self):
        if self.number_of_deals < 10:
            self.level = 0
            self.level_pb = self.number_of_deals
        else:
            self.level = self.number_of_deals // 10
            self.level_pb = self.number_of_deals % 10

    def load_music(self, dt):
        self.music = SoundLoader.load("sounds/the-ghost-of-shepard.ogg")
        self.music_list.append(self.music)

class GameScreen(Screen):
    bet_layout = ObjectProperty(None)
    size_x = NumericProperty(0)
    size_y = NumericProperty(0)
    deck = ObjectProperty()
    deck_start_x = NumericProperty(0)
    deck_start_y = NumericProperty(0)
    player_card_start_x = NumericProperty(0)
    player_card_start_y = NumericProperty(0)
    shift = NumericProperty(0)
    player = ObjectProperty()
    player_score = NumericProperty(0)
    player_hand = ListProperty([])
    dealer_card_start_x = NumericProperty(0)
    dealer_card_start_y = NumericProperty(0)
    dealer = ObjectProperty()
    dealer_score = NumericProperty(0)
    dealer_hand = ListProperty([])
    bet = NumericProperty(0)
    dealed = BooleanProperty(False)
    player_busted = BooleanProperty(False)
    result_label = StringProperty("")
    chip_btns_disabled = BooleanProperty(False)
    blackjack = BooleanProperty(False)
    double_bet = BooleanProperty(False)
    enter_screen = BooleanProperty(False)
    bel = StringProperty("")
    alert_dialog = None
    dialog = None
    level_up_dialog = None

    def animate_bet_info_label(self):
        anim = Animation(pos_hint={"center_x": 0.5, "center_y": 0.22}, duration=3)
        anim += Animation(pos_hint={"center_x": 0.5, "center_y": 0.2}, duration=3)
        anim.repeat = True
        anim.start(self.ids.bet_info_label)

    def on_pre_enter(self):
        self.size_x, self.size_y = Window.size
        self.player_card_start_x = dp(130)
        self.player_card_start_y = dp(180)
        self.dealer_card_start_x = dp(130)
        self.dealer_card_start_y = dp(420)
        self.shift = dp(20)
        self.deck_start_x = self.size_x * 0.8
        self.deck_start_y = self.size_y * 0.9
        self.game.calc_level()
        if not self.enter_screen:
            self.animate_bet_info_label()
            self.game.music.loop = True
            self.game.music.play()
            self.enter_screen = True

    def chip_btn_press(self, bet, image):
        if self.game.money >= (bet + self.bet):
            self.bet += bet
            self.bet_layout.image = image
            self.bet_layout.bet = f"{self.bet}"
            self.bet_layout.opacity = 1
            self.ids.btn_clear.opacity = 1
            self.ids.btn_deal.opacity = 1
            self.ids.bet_info_label.opacity = 0
            self.game.sound_chip.play()
        else:
            self.show_alert_dialog()

    def clear_btn_press(self):
        self.bet = 0
        self.bet_layout.opacity = 0
        self.ids.btn_clear.opacity = 0
        self.ids.btn_deal.opacity = 0
        self.chip_btns_disabled = False
        self.game.sound_click.play()

    def deal_btn_press(self):
        self.chip_btns_disabled = True
        self.chip_bet_animate()
        self.ids.btn_clear.opacity = 0
        self.ids.btn_deal.opacity = 0
        self.game.sound_click.play()

    def chip_bet_animate(self):
        anim = Animation(pos_hint={"center_x": 0.1, "center_y": 0.7}, duration=0.3)
        anim.bind(on_complete=lambda a, w: self.start())
        anim.start(self.bet_layout)

    def start(self):
        self.deck = Deck()
        self.player = Hand()
        self.dealer = Hand()
        self.ids.chip_box.opacity = 0
        self.player.add_card(self.deck.deal())
        for i in range(2):
            self.dealer.add_card(self.deck.deal())
        self.draw_player_hand(0)
        self.dealer_score = self.dealer.calc_hand()
        self.game.money -= self.bet

    def draw_player_card(self, card_number):
        card = Card(
            self.player.cards[card_number][0],
            self.player.cards[card_number][1],
            self.player_card_start_x + self.shift * card_number,
            self.player_card_start_y,
        )
        card.face_up = True
        self.add_widget(card)
        self.player_hand.append(card)
        self.ids.player_score.opacity = 1
        self.player_score = self.player.calc_hand()
        self.check_bust()
        if len(self.player.cards) == 1:
            self.player.add_card(self.deck.deal())
        if self.player.cards.index(self.player.cards[card_number]) + 1 != len(
            self.player.cards
        ):
            self.draw_player_hand(card_number + 1)
        else:
            if len(self.player_hand) == 4:
                self.draw_dealer_hand(0)
            if self.double_bet:
                self.draw_dealer_card(1)

    def animate_player_card(self, card, card_number):
        anim = Animation(
            pos=(
                self.player_card_start_x + self.shift * card_number,
                self.player_card_start_y,
            ),
            duration=0.1,
        )
        anim += Animation(opacity=0.95, duration=0.5)
        anim.bind(on_complete=lambda a, w: self.draw_player_card(card_number))
        self.game.sound_card.play()
        anim.start(card)

    def draw_player_hand(self, card_number):
        card = Card(
            self.player.cards[card_number][0],
            self.player.cards[card_number][1],
            self.deck_start_x,
            self.deck_start_y,
        )
        self.add_widget(card)
        self.player_hand.append(card)
        self.animate_player_card(card, card_number)

    def draw_dealer_card(self, card_number):
        card = Card(
            self.dealer.cards[card_number][0],
            self.dealer.cards[card_number][1],
            self.dealer_card_start_x + self.shift * card_number,
            self.dealer_card_start_y,
        )
        if self.dealer.cards.index(self.dealer.cards[card_number]) != 1 or self.dealed:
            card.face_up = True
        self.add_widget(card)
        self.dealer_hand.append(card)
        if not self.player_busted and len(self.dealer_hand) == 4:
            self.check_blackjack()
        self.dealer_score = self.dealer.calc_hand()
        if self.dealer.cards.index(self.dealer.cards[card_number]) + 1 != len(
            self.dealer.cards
        ):
            self.draw_dealer_hand(card_number + 1)
        if (
            self.dealed
            and not self.player_busted
            and not self.blackjack
            and self.dealer_score < 17
        ):
            self.dealer.add_card(self.deck.deal())
            self.draw_dealer_hand(card_number + 1)
        elif (
            self.dealed
            and not self.player_busted
            and not self.blackjack
            and self.dealer_score >= 17
        ):
            self.ids.dealer_score.opacity = 1
            self.get_winner()

    def animate_dealer_card(self, card, card_number):
        anim = Animation(
            pos=(
                self.dealer_card_start_x + self.shift * card_number,
                self.dealer_card_start_y,
            ),
            duration=0.1,
        )
        anim += Animation(opacity=0.95, duration=0.5)
        anim.bind(on_complete=lambda a, w: self.draw_dealer_card(card_number))
        self.game.sound_card.play()
        anim.start(card)

    def draw_dealer_hand(self, card_number):
        card = Card(
            self.dealer.cards[card_number][0],
            self.dealer.cards[card_number][1],
            self.deck_start_x,
            self.deck_start_y,
        )
        self.add_widget(card)
        self.dealer_hand.append(card)
        self.animate_dealer_card(card, card_number)

    def animate_result_label(self):
        anim = Animation(font_size=sp(30), duration=0.2)
        anim += Animation(opacity=0.95, duration=3)
        anim.bind(on_complete=lambda a, w: self.reset())
        if self.game.number_of_deals < 1001:
            self.game.number_of_deals += 1
        save(
            "saves",
            money=self.game.money,
            deals=self.game.number_of_deals,
        )
        self.game.calc_level()
        anim.start(self.ids.result_label)

    def check_blackjack(self):
        if self.player_score == 21 and self.dealer_score == 21:
            self.dealed = True
            self.blackjack = True
            self.game.money += self.bet
            self.result_label = "Push"
            self.draw_dealer_card(1)
            self.ids.dealer_score.opacity = 1
            self.game.sound_push.play()
            self.animate_result_label()
        elif self.player_score == 21:
            self.dealed = True
            self.blackjack = True
            self.game.money += int(self.bet * 5 / 2)
            self.result_label = f"Blackjack +{int(self.bet*3/2)}"
            self.draw_dealer_card(1)
            self.ids.dealer_score.opacity = 1
            self.game.sound_blackjack.play()
            self.animate_result_label()
        elif self.dealer_score == 21:
            self.dealed = True
            self.blackjack = True
            self.draw_dealer_card(1)
            self.ids.dealer_score.opacity = 1
            self.result_label = "Lost"
            self.game.sound_lost.play()
            self.animate_result_label()
        else:
            self.ids.btn_stand.opacity = 1
            self.ids.btn_double.opacity = 1
            self.ids.btn_hit.opacity = 1

    def check_bust(self):
        if self.player_score > 21:
            self.player_busted = True
            self.result_label = "Bust"
            self.ids.dealer_score.opacity = 1
            self.draw_dealer_card(1)
            self.game.sound_lost.play()
            self.animate_result_label()

    def get_winner(self):
        if self.dealer_score > 21:
            self.result_label = f"Win +{self.bet}"
            self.game.money += self.bet * 2
            self.game.sound_win.play()
        elif self.player_score > self.dealer_score:
            self.result_label = f"Win +{self.bet}"
            self.game.money += self.bet * 2
            self.game.sound_win.play()
        elif self.player_score == self.dealer_score:
            self.result_label = "Push"
            self.game.money += self.bet
            self.game.sound_push.play()
        else:
            self.result_label = "Lost"
            self.game.sound_lost.play()
        self.animate_result_label()

    def reset(self):
        self.bet = 0
        cards = self.player_hand + self.dealer_hand
        self.clear_widgets(children=cards)
        self.player_hand.clear()
        self.dealer_hand.clear()
        self.bet_layout.opacity = 0
        self.bet_layout.pos_hint = {"center_x": 0.5, "center_y": 0.4}
        self.ids.btn_stand.opacity = 0
        self.ids.btn_double.opacity = 0
        self.ids.btn_hit.opacity = 0
        self.ids.player_score.opacity = 0
        self.ids.dealer_score.opacity = 0
        self.result_label = ""
        self.ids.result_label.font_size = sp(14)
        self.player_busted = False
        self.double_bet = False
        self.dealed = False
        self.blackjack = False
        if self.game.number_of_deals % 10 == 0:
            self.show_level_up_dialog()
        if self.game.money > 0:
            self.ids.chip_box.opacity = 1
            self.ids.bet_info_label.opacity = 1
            self.chip_btns_disabled = False
        else:
            self.show_dialog()

    def hit_btn_press(self):
        self.game.sound_click.play()
        self.dealed = True
        self.ids.btn_double.opacity = 0
        self.player.add_card(self.deck.deal())
        card_number = len(self.player.cards) - 1
        self.draw_player_hand(card_number)

    def stand_btn_press(self):
        self.dealed = True
        self.ids.btn_stand.opacity = 0
        self.ids.btn_double.opacity = 0
        self.ids.btn_hit.opacity = 0
        self.ids.dealer_score.opacity = 1
        self.draw_dealer_card(1)

    def double_btn_press(self):
        if self.game.money >= self.bet:
            self.dealed = True
            self.double_bet = True
            self.game.money -= self.bet
            self.bet *= 2
            self.bet_layout.bet = f"{self.bet}"
            self.ids.btn_stand.opacity = 0
            self.ids.btn_double.opacity = 0
            self.ids.btn_hit.opacity = 0
            self.player.add_card(self.deck.deal())
            self.draw_player_hand(2)
        else:
            self.show_alert_dialog()
    
    def show_alert_dialog(self):
        if not self.alert_dialog:
            self.alert_dialog = MyMDDialog(
                radius=[36, 36, 36, 36],
                icon="exclamation-thick",
                icon_color="orange",
                text="Not enough chips for bet",
                font_size=sp(16),
            )
        self.alert_dialog.open()

    def show_dialog(self):
        if not self.dialog:
            self.dialog = MyMDDialog(
                auto_dismiss=False,
                title="You are bankrupt :(",
                radius=[36, 36, 36, 36],
                icon="exclamation-thick",
                icon_color="orange",
                text="Rabbit's Casino thanks you for playing!",
                font_size=sp(16),
                btn_pos="center",
                buttons=[
                    MDRaisedButton(
                        size_hint_x=0.4,
                        text="Exit Game",
                        font_name="Rabbit",
                        md_bg_color="DC8C1B",
                        font_size=sp(12),
                        on_release=lambda _: self.exit_btn_press(),
                    ),
                    MDRaisedButton(
                        size_hint_x=0.4,
                        text="Restart",
                        font_name="Rabbit",
                        md_bg_color="1C88E0",
                        on_release=lambda _: self.restart_btn_press(),
                    ),
                ],
            )
        self.dialog.open()

    def exit_btn_press(self):
        self.game.sound_click.play()
        self.dialog.dismiss()
        sys.exit()

    def restart_btn_press(self):
        self.game.sound_click.play()
        self.dialog.dismiss()
        self.game.money = 2000
        self.game.number_of_deals = 0
        save(
            "saves",
            money=self.game.money,
            deals=self.game.number_of_deals,
        )
        self.game.calc_level()
        self.game.get_screen("themes_scr").card_back_0_press()
        self.game.get_screen("themes_scr").card_face_0_press()
        self.ids.chip_box.opacity = 1
        self.ids.bet_info_label.opacity = 1
        self.chip_btns_disabled = False

    def themes_btn_press(self):
        self.game.sound_click.play()
        self.game.current = "themes_scr"

    def settings_btn_press(self):
        self.game.sound_click.play()
        self.game.current = "settings_scr"

    def show_level_up_dialog(self):
        if not self.level_up_dialog:
            self.level_up_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[36, 36, 36, 36],
                title="You leveled up!",
                type="custom",
                content_cls=RewardLayout(),
                btn_pos="center",
                buttons=[
                    MDRaisedButton(
                        text="Claim",
                        font_name="Rabbit",
                        md_bg_color="1C88E0",
                        on_release=lambda _: self.claim_btn_press(),
                    ),
                ],
            )
        self.level_up_dialog.open()

    def claim_btn_press(self):
        self.game.sound_click.play()
        self.game.money += self.game.level * 10
        save(
            "saves",
            money=self.game.money,
            deals=self.game.number_of_deals,
        )
        self.level_up_dialog.dismiss()


class BlackjackApp(MDApp):
    version = StringProperty("")

    def build(self):
        self.version = VERSION

        BlackjackApp.game = Game(transition=FadeTransition())
        self.game.add_widget(LoadingScreen(name="loading_scr"))
        self.game.add_widget(GameScreen(name="game_scr"))
        self.game.add_widget(SettingsScreen(name="settings_scr"))
        self.game.add_widget(ThemesScreen(name="themes_scr"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rabbit",
        fn_regular="fonts/Rubik-Regular.ttf",
        fn_bold="fonts/Rubik-Medium.ttf",
        fn_italic="fonts/Limelight.ttf",
    )
    BlackjackApp().run()
